# Profile page: Gustavo Zach Vargas

http://gzachv.gitlab.io/

## Description
Simple HTML profile page website served by [Gitlab pages](https://about.gitlab.com/features/pages/).

## Pipeline and deployment
The deployment of the webpage is managed by [Gitlab CI artifacts](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html). The current pipeline status:

[![pipeline status](https://gitlab.com/gzachv/gzachv.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/gzachv/gzachv.gitlab.io/commits/master)

---

Original template from [Creative Tim](https://www.creative-tim.com/)'s [Now UI kit](https://demos.creative-tim.com/now-ui-kit/index.html).
